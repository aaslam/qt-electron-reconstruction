# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

# Simple script to run a
# Calo/Tracking/Egamma job
#
# Usefull for quick testing
# run with
#
# athena --CA runEgammaOnly.py
# or
# python runEgammaOnly.py

import sys


def _run():
    from AthenaConfiguration.AllConfigFlags import initConfigFlags
    flags = initConfigFlags()
    # input
    from AthenaConfiguration.TestDefaults import defaultTestFiles
    flags.Exec.MaxEvents = 20
    print(defaultTestFiles)
    flags.Input.Files = defaultTestFiles.AOD_RUN3_MC
    from AthenaConfiguration.Enums import ProductionStep
    flags.Common.ProductionStep = ProductionStep.Reconstruction
    from SGComps.AddressRemappingConfig import InputRenameCfg
    from AthenaConfiguration.DetectorConfigFlags import setupDetectorFlags

    # output
    #flags.Output.ESDFileName = "myESD.pool.root"
    flags.Output.AODFileName = "myAOD.pool.root"

    # uncomment given something like export ATHENA_CORE_NUMBER=2
    # flags.Concurrency.NumThreads = 2

    # Setup detector flags
    from AthenaConfiguration.DetectorConfigFlags import setupDetectorFlags
    setupDetectorFlags(flags, None, use_metadata=True,
                       toggle_geometry=True, keep_beampipe=True)

    # # egamma Only
    from egammaConfig.egammaOnlyFromRawFlags import egammaOnlyFromRaw
    egammaOnlyFromRaw(flags) 

    flags.lock()
    # from RecJobTransforms.RecoSteering import RecoSteering
    # acc = RecoSteering(flags)
    from AthenaConfiguration.MainServicesConfig import MainServicesCfg
    acc = MainServicesCfg(flags)
    
    from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg 
    acc.merge(PoolReadCfg(flags))
    
    from LArCabling.LArCablingConfig import LArOnOffIdMappingCfg
    acc.merge(LArOnOffIdMappingCfg(flags))
    
    from AtlasGeoModel.GeoModelConfig import GeoModelCfg
    acc.merge (GeoModelCfg (flags))
    
    from LArGeoAlgsNV.LArGMConfig import LArGMCfg
    from TileGeoModel.TileGMConfig import TileGMCfg
    acc.merge(LArGMCfg(flags))
    acc.merge(TileGMCfg(flags))
    
    from LArCabling.LArCablingConfig import LArOnOffIdMappingCfg
    acc.merge(LArOnOffIdMappingCfg(flags))

    from egammaAlgs.egammaRecBuilderConfig import (
        egammaRecBuilderCfg)
    acc.merge(egammaRecBuilderCfg(flags))

    # from egammaAlgs.egammaSuperClusterBuilderConfig import (
    #     electronSuperClusterBuilderCfg, photonSuperClusterBuilderCfg)
    # acc.merge(electronSuperClusterBuilderCfg(flags))
    # acc.merge(photonSuperClusterBuilderCfg(flags))

    from egammaAlgs.topoEgammaBuilderConfig import (
        topoEgammaBuilderCfg)
    acc.merge(topoEgammaBuilderCfg(flags))

    #from egammaAlgs.egammaLargeClusterMakerAlgConfig import (
    #    egammaLargeClusterMakerAlgCfg)
    #acc.merge(egammaLargeClusterMakerAlgCfg(flags))



    # Special message service configuration
    from Digitization.DigitizationSteering import DigitizationMessageSvcCfg
    acc.merge(DigitizationMessageSvcCfg(flags))

    from AthenaConfiguration.Utils import setupLoggingLevels
    setupLoggingLevels(flags, acc)

    # Print reco domain status
    from RecJobTransforms.RecoConfigFlags import printRecoFlags
    printRecoFlags(flags)

    # running
    statusCode = acc.run()
    return statusCode


if __name__ == "__main__":
    statusCode = None
    statusCode = _run()
    assert statusCode is not None, "Issue while running"
    sys.exit(not statusCode.isSuccess())
