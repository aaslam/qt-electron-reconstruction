#!/bin/env python

# NOTE you need FIRST to setup AthAnalysis! Do like this:
# 
# asetup AthAnalysis,23.0,latest
# 
# Then you are free to run this script
# Methods for Electron particle:

# https://acode-browser.usatlas.bnl.gov/lxr/source/athena/Event/xAOD/xAODEgamma/xAODEgamma/versions/EgammaAuxVariables_v3.def
# https://acode-browser.usatlas.bnl.gov/lxr/source/athena/Event/xAOD/xAODEgamma/xAODEgamma/versions/ElectronAuxContainer_v3.h
# https://acode-browser.usatlas.bnl.gov/lxr/source/athena/Event/xAOD/xAODCaloEvent/xAODCaloEvent/versions/CaloCluster_v1.h#0329
# https://acode-browser.usatlas.bnl.gov/lxr/source/athena/Event/xAOD/xAODTracking/xAODTracking/versions/TrackParticle_v1.h#0058


print('Load Root...')
import ROOT as r
print('xAOD init')
r.xAOD.Init().ignore()

print('Open files')
ch = r.TChain('CollectionTree')


## This is the file name you want to open.

# Input file location 
#m_FILE = '/eos/atlas/user/f/fernando/mc21_13p6TeV.900333.PG_single_electron_egammaET.recon.AOD.e8453_s3873_r14136/AOD.31091541._001650.pool.root.1'

# Output file location
m_FILE = '/afs/cern.ch/user/a/aaslam/QT/myAOD.pool.root'


ch.Add(m_FILE)


#make transient tree
data = r.xAOD.MakeTransientTree(ch)

print('Read first event')
# Read first event
data.GetEntry(0)

print('Read electrons on first event')
Electrons = data.Electrons
el = Electrons[0]
cl = el.caloCluster()
print('Done')
print('data is your tree')

# a Simple loop over all events only yo see the number of electrons on each
for i in range(10):
    data.GetEntry(i)
    print('Entry'+str(i)+' -> Electrons: = ' + str(data.Electrons.size()))


Electrons = data.Electrons
el = data.Electrons[0]

print('Electron pt = %.2f' %el.pt())
print('Electron eta = %.2f' %el.eta())
print('Electron phi = %.2f' %el.phi())


