# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

# Simple script to run a
# Calo/Tracking/Egamma job
#
# Usefull for quick testing
# run with
#
# athena --CA runEgammaOnly.py
# or
# python runEgammaOnly.py

from LArCabling.LArCablingConfig import LArOnOffIdMappingCfg
import sys


def _run():
    from AthenaConfiguration.AllConfigFlags import initConfigFlags
    flags = initConfigFlags()
    # input
    from AthenaConfiguration.TestDefaults import defaultTestFiles
    flags.Exec.MaxEvents = 1000
    #print(defaultTestFiles)        
    #flags.Input.Files = defaultTestFiles.AOD_RUN3_MC
    flags.Input.Files = ["/eos/atlas/user/f/fernando/mc21_13p6TeV.900333.PG_single_electron_egammaET.recon.AOD.e8453_s3873_r14136/AOD.31091541._001650.pool.root.1"]
    
            
    from AthenaConfiguration.Enums import ProductionStep
    flags.Common.ProductionStep = ProductionStep.Reconstruction
    # Disable detectors we do not need
    flags.Detector.GeometryMuon = False
    flags.Detector.EnableAFP = False
    flags.Detector.EnableLucid = False
    flags.Detector.EnableZDC = False
    flags.Input.isMC = True
    # we will need to remap inputs
    from SGComps.AddressRemappingConfig import InputRenameCfg

    # output
    # flags.Output.ESDFileName = "myESD.pool.root"
    flags.Output.AODFileName = "myAOD.pool.root"

    # uncomment given something like export ATHENA_CORE_NUMBER=2
    # flags.Concurrency.NumThreads = 2

    # Setup detector flags
    from AthenaConfiguration.DetectorConfigFlags import setupDetectorFlags
    setupDetectorFlags(flags, None, use_metadata=True,
                       toggle_geometry=True, keep_beampipe=True)

    flags.lock()

    from AthenaConfiguration.MainServicesConfig import MainServicesCfg
    acc = MainServicesCfg(flags)

    from AtlasGeoModel.GeoModelConfig import GeoModelCfg
    acc.merge(GeoModelCfg(flags))

    from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
    acc.merge(PoolReadCfg(flags))

    if flags.Detector.EnablePixel:
        from PixelGeoModel.PixelGeoModelConfig import PixelReadoutGeometryCfg
        acc.merge(PixelReadoutGeometryCfg(flags))
    if flags.Detector.EnableSCT:
        from SCT_GeoModel.SCT_GeoModelConfig import SCT_ReadoutGeometryCfg
        acc.merge(SCT_ReadoutGeometryCfg(flags))
    if flags.Detector.EnableTRT:
        from TRT_GeoModel.TRT_GeoModelConfig import TRT_ReadoutGeometryCfg
        acc.merge(TRT_ReadoutGeometryCfg(flags))

    if flags.Detector.EnableLAr:
        from LArGeoAlgsNV.LArGMConfig import LArGMCfg
        acc.merge(LArGMCfg(flags))
        from LArCabling.LArCablingConfig import LArOnOffIdMappingCfg
        acc.merge(LArOnOffIdMappingCfg(flags))
        from LArBadChannelTool.LArBadChannelConfig import LArBadChannelCfg, LArBadFebCfg
        acc.merge(LArBadChannelCfg(flags))
        acc.merge(LArBadFebCfg(flags))

    if flags.Detector.EnableTile:
        from TileGeoModel.TileGMConfig import TileGMCfg
        acc.merge(TileGMCfg(flags))
        from TileConditions.TileCablingSvcConfig import TileCablingSvcCfg
        acc.merge(TileCablingSvcCfg(flags))

    from egammaAlgs.egammaTopoClusterCopierConfig import (
        egammaTopoClusterCopierCfg)
    acc.merge(egammaTopoClusterCopierCfg(flags,name='',InputTopoCollection='CaloCalTopoClusters'))

    # Algorithms to run
    from egammaAlgs.egammaRecBuilderConfig import (
        egammaRecBuilderCfg)
    acc.merge(egammaRecBuilderCfg(flags))

    from egammaAlgs.egammaSuperClusterBuilderConfig import (
        electronSuperClusterBuilderCfg, photonSuperClusterBuilderCfg)
    acc.merge(electronSuperClusterBuilderCfg(flags))
    acc.merge(photonSuperClusterBuilderCfg(flags))

    from egammaAlgs.topoEgammaBuilderConfig import (
        topoEgammaBuilderCfg)
    acc.merge(topoEgammaBuilderCfg(flags))

    # from egammaAlgs.egammaLargeClusterMakerAlgConfig import (
    #    egammaLargeClusterMakerAlgCfg)
    # acc.merge(egammaLargeClusterMakerAlgCfg(flags))

    # Special message service configuration
    from Digitization.DigitizationSteering import DigitizationMessageSvcCfg
    acc.merge(DigitizationMessageSvcCfg(flags))

    from AthenaConfiguration.Utils import setupLoggingLevels
    setupLoggingLevels(flags, acc)

    from egammaConfig.egammaOutputConfig import (
        egammaOutputCfg)
    acc.merge(egammaOutputCfg(flags))

    # running
    statusCode = acc.run()
    return statusCode


if __name__ == "__main__":
    statusCode = None
    statusCode = _run()
    assert statusCode is not None, "Issue while running"
    sys.exit(not statusCode.isSuccess())
